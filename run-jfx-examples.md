# Usando Maven
## Repo de openjfx
### Requisitos
1. Tener Java 13 instalado
2. Tener Maven instalado (al menos **v.3.5.4**)

### Pasos
1. Clonar el repo [https://github.com/openjfx/samples.git](https://github.com/openjfx/samples.git)
2. Definir la variable de entorno de Java (JAVA_HOME) apuntando a la versión 13:
```bash
export JAVA_HOME="<ruta-home>/openjdk/jdk-13.0.2"
```
3. Seguir los pasos en [https://github.com/openjfx/samples/tree/master/HelloFX/Maven](https://github.com/openjfx/samples/tree/master/HelloFX/Maven):
```bash
cd HelloFX/Maven/hellofx

mvn clean javafx:run
```
Una vez que Maven descargue todas las dependencias que necesita, abrirá una ventana
con el mensaje:
```text
Hello, JavaFX 13, running on Java 13.0.2.
```

# Referencias
* [Run HelloWorld using Maven](https://openjfx.io/openjfx-docs/#maven)
* [HelloFX Maven README](https://github.com/openjfx/samples/tree/master/HelloFX/Maven)
* [OpenjFX samples](https://github.com/openjfx/samples)
* [Switch between multiple java versions](https://askubuntu.com/questions/740757/switch-between-multiple-java-versions)
* [Set JAVA_HOME on Linux](https://www.baeldung.com/java-home-on-windows-7-8-10-mac-os-x-linux)
