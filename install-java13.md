# Guía para instalar Java 13 sobre Linux (Debian)

## Descargar JDK 13.0.2 (64-bits)
1. Ingresa a la página [http://jdk.java.net/13/](http://jdk.java.net/13/) donde encontrarás la versión más reciente de Java 13.

2. Ubica la sección open-source de los compilados en la versión para **Linux/x64** 
 y descarga el archivo con terminación [**tar.gz**](https://download.java.net/java/GA/jdk13.0.2/d4173c853231432d94f001e99d882ca7/8/GPL/openjdk-13.0.2_linux-x64_bin.tar.gz).


3. Crea una carpeta en tu home llamada **openjdk** y mueve el archivo descargado
(openjdk-13.0.2_linux-x64_bin.tar.gz) a esa carpeta.

4. Extrae el contenido del archivo.
```bash
tar xvf <nombre-del-archivo>.tar.gz
```
Se creará un directorio con el nombre **jdk-13.0.2**

5. Exporta a la variable PATH la ruta donde se encuentran los binarios:
```bash
export PATH=$PWD/jdk-13.0.2/bin:$PATH
```

6. Verifica la versión de Java:
```bash
$ java -version
openjdk version "13.0.2" 2020-01-14
OpenJDK Runtime Environment (build 13.0.2+8)
OpenJDK 64-Bit Server VM (build 13.0.2+8, mixed mode, sharing)
```

## Referencias
*  [https://adoptopenjdk.net/installation.html#x64_linux-jdk](https://adoptopenjdk.net/installation.html#x64_linux-jdk)
*  [https://openjdk.java.net/install/index.html](https://openjdk.java.net/install/index.html)